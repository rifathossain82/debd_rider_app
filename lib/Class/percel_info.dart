class PercelInfo {
  String id;
  String shopname;
  String userId;
  String customername;
  String cusPhone;
  String cusAddress;
  String marchantInvId;
  String weight;
  String cashCollectionAmount;
  String sellPrice;
  String deliveryArea;
  String pickupArea;
  String instruction;
  String areaStatus;
  String deliveryCharge;
  String codCharge;
  String totalPayable;
  String orderStatus;
  String pickupStatus;
  Null pickupIssue;
  String deliveryStatus;
  String transferStatus;
  String returnTransferStatus;
  String returnStatus;
  String pickupmanId;
  String returnPickupmanId;
  String deliverymanId;
  String trackingId;
  String paidAmount;
  String transactionInfo;
  String paymentStatus;
  String paymentDate;
  String orderDate;
  String orderCompletedDate;
  String branchId;
  String transferBranchId;
  String returnTransferBranchId;
  String district;
  String deliveryType;
  String notification;
  String returnPickupNotification;
  String note;
  String pickupAddress;
  String deliveryOtp;

  PercelInfo(
      {this.id,
        this.shopname,
        this.userId,
        this.customername,
        this.cusPhone,
        this.cusAddress,
        this.marchantInvId,
        this.weight,
        this.cashCollectionAmount,
        this.sellPrice,
        this.deliveryArea,
        this.pickupArea,
        this.instruction,
        this.areaStatus,
        this.deliveryCharge,
        this.codCharge,
        this.totalPayable,
        this.orderStatus,
        this.pickupStatus,
        this.pickupIssue,
        this.deliveryStatus,
        this.transferStatus,
        this.returnTransferStatus,
        this.returnStatus,
        this.pickupmanId,
        this.returnPickupmanId,
        this.deliverymanId,
        this.trackingId,
        this.paidAmount,
        this.transactionInfo,
        this.paymentStatus,
        this.paymentDate,
        this.orderDate,
        this.orderCompletedDate,
        this.branchId,
        this.transferBranchId,
        this.returnTransferBranchId,
        this.district,
        this.deliveryType,
        this.notification,
        this.returnPickupNotification,
        this.note,
        this.pickupAddress,
        this.deliveryOtp});

  PercelInfo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    shopname = json['shopname'];
    userId = json['user_id'];
    customername = json['customername'];
    cusPhone = json['cus_phone'];
    cusAddress = json['cus_address'];
    marchantInvId = json['marchant_inv_id'];
    weight = json['weight'];
    cashCollectionAmount = json['cash_collection_amount'];
    sellPrice = json['sell_price'];
    deliveryArea = json['delivery_area'];
    pickupArea = json['pickup_area'];
    instruction = json['instruction'];
    areaStatus = json['area_status'];
    deliveryCharge = json['delivery_charge'];
    codCharge = json['cod_charge'];
    totalPayable = json['total_payable'];
    orderStatus = json['order_status'];
    pickupStatus = json['pickup_status'];
    pickupIssue = json['pickup_issue'];
    deliveryStatus = json['delivery_status'];
    transferStatus = json['transfer_status'];
    returnTransferStatus = json['return_transfer_status'];
    returnStatus = json['return_status'];
    pickupmanId = json['pickupman_id'];
    returnPickupmanId = json['return_pickupman_id'];
    deliverymanId = json['deliveryman_id'];
    trackingId = json['tracking_id'];
    paidAmount = json['paid_amount'];
    transactionInfo = json['transaction_info'];
    paymentStatus = json['payment_status'];
    paymentDate = json['payment_date'];
    orderDate = json['order_date'];
    orderCompletedDate = json['order_completed_date'];
    branchId = json['branch_id'];
    transferBranchId = json['transfer_branch_id'];
    returnTransferBranchId = json['return_transfer_branch_id'];
    district = json['district'];
    deliveryType = json['delivery_type'];
    notification = json['notification'];
    returnPickupNotification = json['return_pickup_notification'];
    note = json['note'];
    pickupAddress = json['pickup_address'];
    deliveryOtp = json['delivery_otp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['shopname'] = this.shopname;
    data['user_id'] = this.userId;
    data['customername'] = this.customername;
    data['cus_phone'] = this.cusPhone;
    data['cus_address'] = this.cusAddress;
    data['marchant_inv_id'] = this.marchantInvId;
    data['weight'] = this.weight;
    data['cash_collection_amount'] = this.cashCollectionAmount;
    data['sell_price'] = this.sellPrice;
    data['delivery_area'] = this.deliveryArea;
    data['pickup_area'] = this.pickupArea;
    data['instruction'] = this.instruction;
    data['area_status'] = this.areaStatus;
    data['delivery_charge'] = this.deliveryCharge;
    data['cod_charge'] = this.codCharge;
    data['total_payable'] = this.totalPayable;
    data['order_status'] = this.orderStatus;
    data['pickup_status'] = this.pickupStatus;
    data['pickup_issue'] = this.pickupIssue;
    data['delivery_status'] = this.deliveryStatus;
    data['transfer_status'] = this.transferStatus;
    data['return_transfer_status'] = this.returnTransferStatus;
    data['return_status'] = this.returnStatus;
    data['pickupman_id'] = this.pickupmanId;
    data['return_pickupman_id'] = this.returnPickupmanId;
    data['deliveryman_id'] = this.deliverymanId;
    data['tracking_id'] = this.trackingId;
    data['paid_amount'] = this.paidAmount;
    data['transaction_info'] = this.transactionInfo;
    data['payment_status'] = this.paymentStatus;
    data['payment_date'] = this.paymentDate;
    data['order_date'] = this.orderDate;
    data['order_completed_date'] = this.orderCompletedDate;
    data['branch_id'] = this.branchId;
    data['transfer_branch_id'] = this.transferBranchId;
    data['return_transfer_branch_id'] = this.returnTransferBranchId;
    data['district'] = this.district;
    data['delivery_type'] = this.deliveryType;
    data['notification'] = this.notification;
    data['return_pickup_notification'] = this.returnPickupNotification;
    data['note'] = this.note;
    data['pickup_address'] = this.pickupAddress;
    data['delivery_otp'] = this.deliveryOtp;
    return data;
  }
}