class DeliveryData {
  String date;
  String marchantInfo;
  String customerInfo;
  String customerNumber;
  String marchantNumber;
  String trackId;
  String shopname;
  String cashcollection;
  String codCharge;
  String totalPayable;
  String marchantName;
  String paymentinfo;
  String instruction;
  String percelid;
  String status;
  String pickupStatus;
  String deliveryStatus;
  String id;
  String notification;
  String deliveryCharge;
  String deliverymanName;

  DeliveryData(
      {this.date,
        this.marchantInfo,
        this.customerInfo,
        this.customerNumber,
        this.marchantNumber,
        this.trackId,
        this.shopname,
        this.cashcollection,
        this.codCharge,
        this.totalPayable,
        this.marchantName,
        this.paymentinfo,
        this.instruction,
        this.percelid,
        this.status,
        this.pickupStatus,
        this.deliveryStatus,
        this.id,
        this.notification,
        this.deliveryCharge,
        this.deliverymanName});

  DeliveryData.fromJson(Map<String, dynamic> json) {
    date = json['date'];
    marchantInfo = json['marchantInfo'];
    customerInfo = json['customer_info'];
    customerNumber = json['customer_number'];
    marchantNumber = json['marchant_number'];
    trackId = json['track_id'];
    shopname = json['shopname'];
    cashcollection = json['cashcollection'];
    codCharge = json['cod_charge'];
    totalPayable = json['total_payable'];
    marchantName = json['marchant_name'];
    paymentinfo = json['paymentinfo'];
    instruction = json['instruction'];
    percelid = json['percelid'];
    status = json['status'];
    pickupStatus = json['pickup_status'];
    deliveryStatus = json['delivery_status'];
    id = json['id'];
    notification = json['notification'];
    deliveryCharge = json['delivery_charge'];
    deliverymanName = json['deliveryman_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date'] = this.date;
    data['marchantInfo'] = this.marchantInfo;
    data['customer_info'] = this.customerInfo;
    data['customer_number'] = this.customerNumber;
    data['marchant_number'] = this.marchantNumber;
    data['track_id'] = this.trackId;
    data['shopname'] = this.shopname;
    data['cashcollection'] = this.cashcollection;
    data['cod_charge'] = this.codCharge;
    data['total_payable'] = this.totalPayable;
    data['marchant_name'] = this.marchantName;
    data['paymentinfo'] = this.paymentinfo;
    data['instruction'] = this.instruction;
    data['percelid'] = this.percelid;
    data['status'] = this.status;
    data['pickup_status'] = this.pickupStatus;
    data['delivery_status'] = this.deliveryStatus;
    data['id'] = this.id;
    data['notification'] = this.notification;
    data['delivery_charge'] = this.deliveryCharge;
    data['deliveryman_name'] = this.deliverymanName;
    return data;
  }
}