class PickUpData {
  String date;
  String marchantinfo;
  String pickupinfo;
  String customerNumber;
  String marchantNumber;
  String trackid;
  String shopname;
  String cashcollection;
  String codCharge;
  String totalPayable;
  String marchantName;
  String paymentinfo;
  String paymentstatus;
  String instruction;
  String status;
  String pickupStatus;
  String deliveryStatus;
  String id;
  String notification;
  String deliveryCharge;
  String pickupmanName;

  PickUpData(
      {this.date,
        this.marchantinfo,
        this.pickupinfo,
        this.customerNumber,
        this.marchantNumber,
        this.trackid,
        this.shopname,
        this.cashcollection,
        this.codCharge,
        this.totalPayable,
        this.marchantName,
        this.paymentinfo,
        this.paymentstatus,
        this.instruction,
        this.status,
        this.pickupStatus,
        this.deliveryStatus,
        this.id,
        this.notification,
        this.deliveryCharge,
        this.pickupmanName});

  PickUpData.fromJson(Map<String, dynamic> json) {
    date = json['date'];
    marchantinfo = json['marchantinfo'];
    pickupinfo = json['pickupinfo'];
    customerNumber = json['customer_number'];
    marchantNumber = json['marchant_number'];
    trackid = json['trackid'];
    shopname = json['shopname'];
    cashcollection = json['cashcollection'];
    codCharge = json['cod_charge'];
    totalPayable = json['total_payable'];
    marchantName = json['marchant_name'];
    paymentinfo = json['paymentinfo'];
    paymentstatus = json['paymentstatus'];
    instruction = json['instruction'];
    status = json['status'];
    pickupStatus = json['pickup_status'];
    deliveryStatus = json['delivery_status'];
    id = json['id'];
    notification = json['notification'];
    deliveryCharge = json['delivery_charge'];
    pickupmanName = json['pickupman_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date'] = this.date;
    data['marchantinfo'] = this.marchantinfo;
    data['pickupinfo'] = this.pickupinfo;
    data['customer_number'] = this.customerNumber;
    data['marchant_number'] = this.marchantNumber;
    data['trackid'] = this.trackid;
    data['shopname'] = this.shopname;
    data['cashcollection'] = this.cashcollection;
    data['cod_charge'] = this.codCharge;
    data['total_payable'] = this.totalPayable;
    data['marchant_name'] = this.marchantName;
    data['paymentinfo'] = this.paymentinfo;
    data['paymentstatus'] = this.paymentstatus;
    data['instruction'] = this.instruction;
    data['status'] = this.status;
    data['pickup_status'] = this.pickupStatus;
    data['delivery_status'] = this.deliveryStatus;
    data['id'] = this.id;
    data['notification'] = this.notification;
    data['delivery_charge'] = this.deliveryCharge;
    data['pickupman_name'] = this.pickupmanName;
    return data;
  }
}