import 'package:flutter/material.dart';
import 'package:pickupman/Color_Me/Color_me.dart';


class HeaderContainer extends StatelessWidget {
  var text = "Login";

  HeaderContainer(this.text);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.4,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [Color_me.green, Color_me.p_red],
              end: Alignment.bottomCenter,
              begin: Alignment.topCenter),
          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(100))),
      child: Stack(
        children: <Widget>[
          Positioned(
            bottom: 20,
              right: 20,
              child: Text(
            text,
            style: TextStyle(color: Colors.white,fontSize: 20),
          )),
          Center(
              child: Container(
                  width: MediaQuery.of(context).size.width*0.7,
                  height: MediaQuery.of(context).size.height*0.7,
                  child:Image.asset(
                    "assets/logo.png",
                  )
              ),

         /*   child: Row(
              children: <Widget>[
                SizedBox(width: 30,),
                Text(
                  "World",
                  style: TextStyle(
                      fontSize: 40,
                      fontWeight: FontWeight.bold,
                      color: Color_me.white
                  ),
                ),
                Text(
                  "EX",
                  style: TextStyle(
                      fontSize: 50,
                      fontWeight: FontWeight.bold,
                      color: Color_me.red
                  ),
                ),
              ],
            ),*/
          ),
        ],
      ),
    );
  }
}
