import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class KTextFiled extends StatelessWidget {
  const KTextFiled({
    Key key,
    this.controller,
    this.focusNode,
    this.hintText,
    this.labelText,
    this.onChanged,
    this.inputType,
    this.inputAction,
    this.suffix,
    this.maxLine,
    this.minLine,
    this.contentPadding = const EdgeInsets.symmetric(horizontal: 15),
    this.obscureValue,
  }) : super(key: key);

  final TextEditingController controller;
  final FocusNode focusNode;
  final String hintText;
  final String labelText;
  final Function(String value) onChanged;
  final TextInputType inputType;
  final TextInputAction inputAction;
  final Widget suffix;
  final int maxLine;
  final int minLine;
  final EdgeInsets contentPadding;
  final bool obscureValue;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      focusNode: focusNode,
      onChanged: onChanged,
      maxLines: maxLine,
      minLines: minLine,
      keyboardType: inputType,
      textInputAction: inputAction,
      obscureText: obscureValue ?? false,
      decoration: InputDecoration(
        contentPadding: contentPadding,
        hintText: hintText,
        labelText: labelText,
        border: OutlineInputBorder(),
        suffixIcon: suffix,
      ),
    );
  }
}
