import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:pickupman/Color_Me/Color_me.dart';
import 'CustomDialog.dart';
import 'current-data-chart-painter.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:flutter/widgets.dart';

class DashboardCard extends StatefulWidget {


  @override
  _DashboardCardState createState() => _DashboardCardState();
}

class _DashboardCardState extends State<DashboardCard> {
  
  String nameString;
  var data;

  bool loading = true;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }


  @override
  Widget build(BuildContext context) {

    SizeConfig().init(context);

    return Scaffold(

      body: Container(

          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,

          child: Column(

          children: <Widget>[

                  Row(
                    children: <Widget>[

                      Expanded(
                        flex: 1,
                        child:Container(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              _buildItem("Total Due", "", Color_me.purple,Image.asset(
                                "assets/taka_due.png",
                              ),),
                            ],
                          ),
                        ), ),
                      Expanded(
                        flex: 1,
                        child:Container(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              _buildItem("Total Due", "", Color_me.purple,Image.asset(
                                "assets/taka_due.png",
                              ),),
                            ],
                          ),
                        ), ),


                    ],
                  ),
                  Expanded(
                    flex: 1,
                    child:Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          _buildItem("Total Due", "", Color_me.purple,Image.asset(
                            "assets/taka_due.png",
                          ),),
                        ],
                      ),
                    ), )




                ],
              ),


      ),

    );

  }


  Widget _buildDivider() {
    return Flexible(
      flex: 1,
      child: SizedBox(
        width: 5,
        child: Container(
          alignment: Alignment.centerRight,
          height: 70,
          decoration: BoxDecoration(
            border: Border(
              right: BorderSide(
                width: 2,
                color: Colors.black.withOpacity(0.4),
              ),
            ),
          ),
        ),
      ),
    );
  }


  Widget _buildItem(String title, String subtitle, Color chartColor,Image image) {

    return Flexible(
      flex: 5,
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height*0.15,

        child: Card(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: 30,
                  height: 30,
                  child: image
                ),

                SizedBox(width: 10,),
                Expanded(
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        AutoSizeText(
                          title,
                          style: TextStyle(
                              fontSize: 16,
                              /*fontWeight: FontWeight.bold*/
                              color: Colors.black
                          ),
                        ),
                        AutoSizeText(
                          subtitle,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 14,
                            color: Colors.black,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),

      ),
    );
  }

/*  Future<List> get_posts() async {

    final response =
    await http.get("");
    data = json.decode(response.body);
    *//*var nameJson = data['inhouse'];
    nameString = jsonEncode(nameJson);*//* // jsonEncode != .toString()

     print(data);

    loading = false;

    if(mounted){
      setState(() {});
    }


    *//*   Link = data['next_page_url'];
    _refreshController.refreshCompleted();*//*

  }*/



}

class SizeConfig {
  static MediaQueryData _mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double blockSizeHorizontal;
  static double blockSizeVertical;

  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    blockSizeHorizontal = screenWidth / 100;
    blockSizeVertical = screenHeight / 100;
  }
}
