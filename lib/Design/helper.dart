import 'package:flutter/material.dart';

void showSnackBar(BuildContext context, String data, Color bgColor) {
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      content: Text(data, style: TextStyle(color: Colors.white),),
      duration: Duration(seconds: 2),
      backgroundColor: bgColor,
      behavior: SnackBarBehavior.floating,
      margin: EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 15
      ),
    ),
  );
}
