import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pickupman/Class/percel_info.dart';
import 'package:pickupman/Class/pickup_data.dart';
import 'package:pickupman/Color_Me/Color_me.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:pickupman/Design/helper.dart';
import 'package:pickupman/Imp/url.dart';
import 'package:pickupman/main.dart';
import 'package:pickupman/widgets/btn_widget.dart';
import 'package:pickupman/widgets/k_text_field.dart';
import 'package:http/http.dart' as http;

class Pickup_details extends StatefulWidget {
  @override
  _Pickup_detailsState createState() => _Pickup_detailsState();
}

class _Pickup_detailsState extends State<Pickup_details> {
  final GlobalKey<RefreshIndicatorState> refresh =
      GlobalKey<RefreshIndicatorState>();

  PickUpData data;
  PercelInfo percelInfo;
  String parcelId;
  bool loading = true;
  final noteController = TextEditingController();

  @override
  void didChangeDependencies() {
    data = ModalRoute.of(context).settings.arguments;
    parcelId = data.id;
    getParcelInfo();
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
  }

  getParcelInfo() async {
    final response =
        await http.get(main_url + 'fetchPercelInfoById/' + parcelId);
    var data = json.decode(response.body.toString());

    print(data);
    percelInfo = PercelInfo.fromJson(data);

    loading = false;

    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color_me.white,
        title: Text(
          "Percel Details".tr().toString(),
          style: TextStyle(color: Color_me.green, fontWeight: FontWeight.bold),
        ),
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
      ),
      body: Container(
        child: SingleChildScrollView(
          child: loading
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : Column(
                  children: <Widget>[
                    gray_lebel("About Parcel".tr().toString()),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.01,
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              flex: 1,
                              child: Container(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Creation Date".tr().toString(),
                                  style: GoogleFonts.lato(
                                      textStyle: TextStyle(
                                          fontWeight: FontWeight.bold)),
                                ),
                              )),
                          Expanded(
                              flex: 1,
                              child: Container(
                                alignment: Alignment.centerRight,
                                child: SelectableText(
                                  data.date,
                                  textAlign: TextAlign.end,
                                  style: TextStyle(),
                                ),
                              ))
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              flex: 1,
                              child: Container(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Shop Name".tr().toString(),
                                  style: GoogleFonts.lato(
                                      textStyle: TextStyle(
                                          fontWeight: FontWeight.bold)),
                                ),
                              )),
                          Expanded(
                              flex: 1,
                              child: Container(
                                alignment: Alignment.centerRight,
                                child: SelectableText(data.shopname,
                                    textAlign: TextAlign.end),
                              ))
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              flex: 1,
                              child: Container(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Status".tr().toString(),
                                  style: GoogleFonts.lato(
                                      textStyle: TextStyle(
                                          fontWeight: FontWeight.bold)),
                                ),
                              )),
                          Expanded(
                              flex: 1,
                              child: Container(
                                alignment: Alignment.centerRight,
                                child: SelectableText(data.status,
                                    textAlign: TextAlign.end),
                              ))
                        ],
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.01,
                    ),
                    gray_lebel("Pickup Info".tr().toString()),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              flex: 1,
                              child: Container(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Pickup Info".tr().toString(),
                                  style: GoogleFonts.lato(
                                      textStyle: TextStyle(
                                          fontWeight: FontWeight.bold)),
                                ),
                              )),
                          Expanded(
                              flex: 1,
                              child: Container(
                                alignment: Alignment.centerRight,
                                child: SelectableText(data.pickupinfo,
                                    textAlign: TextAlign.end),
                              ))
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              flex: 1,
                              child: Container(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Tracking ID".tr().toString(),
                                  style: GoogleFonts.lato(
                                      textStyle: TextStyle(
                                          fontWeight: FontWeight.bold)),
                                ),
                              )),
                          Expanded(
                              flex: 1,
                              child: Container(
                                alignment: Alignment.centerRight,
                                child: SelectableText(data.trackid,
                                    textAlign: TextAlign.end),
                              ))
                        ],
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.01,
                    ),
                    gray_lebel("Payment Info".tr().toString()),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              flex: 1,
                              child: Container(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Payment Info".tr().toString(),
                                  style: GoogleFonts.lato(
                                      textStyle: TextStyle(
                                          fontWeight: FontWeight.bold)),
                                ),
                              )),
                          Expanded(
                              flex: 1,
                              child: Container(
                                alignment: Alignment.centerRight,
                                child: SelectableText(data.paymentinfo,
                                    textAlign: TextAlign.end),
                              ))
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              flex: 1,
                              child: Container(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Instruction".tr().toString(),
                                  style: GoogleFonts.lato(
                                      textStyle: TextStyle(
                                          fontWeight: FontWeight.bold)),
                                ),
                              )),
                          Expanded(
                              flex: 1,
                              child: Container(
                                alignment: Alignment.centerRight,
                                child: SelectableText(data.instruction,
                                    textAlign: TextAlign.end),
                              ))
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              flex: 1,
                              child: Container(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Payment Status".tr().toString(),
                                  style: GoogleFonts.lato(
                                      textStyle: TextStyle(
                                          fontWeight: FontWeight.bold)),
                                ),
                              )),
                          Expanded(
                              flex: 1,
                              child: Container(
                                alignment: Alignment.centerRight,
                                child: SelectableText(data.paymentinfo,
                                    textAlign: TextAlign.end),
                              ))
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 15,
                        vertical: 8,
                      ),
                      child: ButtonWidget(
                        btnText: 'Pickup and send to inhouse',
                        onClick: () {
                          showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (context) => AlertDialog(
                              title: Text('Pickup Confirmation'),
                              content: KTextFiled(
                                controller: noteController,
                                hintText: 'Type a status note',
                                labelText: 'Status Note',
                                minLine: 2,
                                contentPadding: EdgeInsets.all(15),
                              ),
                              actions: [
                                TextButton(
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                    child: Text('Cancel')),
                                TextButton(
                                    onPressed: sendToInHouse,
                                    child: Text('Send')),
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 15,
                        vertical: 8,
                      ),
                      child: ButtonWidget(
                        btnText: 'Pickup and send to delivery',
                        onClick: () {
                          showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (context) => AlertDialog(
                              title: Text('Pickup Confirmation'),
                              content: KTextFiled(
                                controller: noteController,
                                hintText: 'Type a status note',
                                labelText: 'Status Note',
                                minLine: 2,
                                contentPadding: EdgeInsets.all(15),
                              ),
                              actions: [
                                TextButton(
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                    child: Text('Cancel')),
                                TextButton(
                                    onPressed: sendToDelivery,
                                    child: Text('Send')),
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                    const SizedBox(
                      height: 24,
                    ),
                  ],
                ),
        ),
      ),
    );
  }

  Widget gray_lebel(String txt) {
    return Container(
      padding: EdgeInsets.only(left: 10),
      alignment: Alignment.centerLeft,
      color: Colors.black12,
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.04,
      child: Text(txt,
          style: GoogleFonts.lato(textStyle: TextStyle(color: Color_me.grey))),
    );
  }

  void sendToInHouse() async {
    var map = {};
    map['percel_status'] = '1';
    map['note'] = noteController.text;

    print('Request Body: $map');
    final response = await http
        .post(main_url + 'updatePickupPercelsByStaff/' + parcelId, body: map);

    var data = json.decode(response.body.toString());
    print(data);

    if (data['success'].toString().contains('true')) {
      showSnackBar(context, data['messages'], Color_me.successColor);
      Navigator.pop(context);
    } else {
      showSnackBar(context, data['messages'], Color_me.failedColor);
    }

    getParcelInfo();

    if (mounted) {
      setState(() {});
    }

    Navigator.pop(context);
  }

  void sendToDelivery() async {
    String userId = prefs.getString('uid1');

    var map = {};
    map['percel_status'] = '1';
    map['note'] = noteController.text;
    map['deliveryman_id'] = userId;
    map['order_status'] = '3';

    print('Request Body: $map');
    final response = await http.post(
        main_url + 'updatePickupDeliveryPercelsByStaff/' + parcelId,
        body: map);

    var data = json.decode(response.body.toString());
    print(data);

    if (data['success'].toString().contains('true')) {
      showSnackBar(context, data['messages'], Color_me.successColor);
      Navigator.pop(context);
    } else {
      showSnackBar(context, data['messages'], Color_me.failedColor);
    }

    getParcelInfo();

    if (mounted) {
      setState(() {});
    }

    Navigator.pop(context);
  }
}
