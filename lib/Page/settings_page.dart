import 'package:auto_size_text/auto_size_text.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pickupman/Color_Me/Color_me.dart';
import 'package:pickupman/Imp/route.dart';

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        backgroundColor: Color_me.white,
        leading: GestureDetector(
          onTap: ()
          {
            Navigator.pop(context);
            Navigator.of(context).pushNamed(HOME);

          },
          child: Icon(Icons.arrow_back,color: Colors.black,),
        ),
        title: Container(
            child: Row(
              children: <Widget>[
                AutoSizeText(
                  "Settings".tr().toString(),
                  style: TextStyle(
                      color: Color_me.green, fontWeight: FontWeight.w900),
                ),

              ],
            )),
      ),

      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,

        padding: EdgeInsets.all(10),
        child:Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          alignment: Alignment.centerLeft,
          child: Column(
            children: <Widget>[
              SizedBox(
                height: MediaQuery.of(context).size.height*0.02,

              ),
            Container(

                width: MediaQuery.of(context).size.width,
                alignment: Alignment.centerLeft,

              child:Text("Change Language".tr().toString(),
                textAlign: TextAlign.start,
                style: TextStyle(

                    color: Color_me.green,
                  fontWeight: FontWeight.w900

                ),),
            ),

           Container(
             child:      Row(
               children: <Widget>[
                 SizedBox(
                   width: MediaQuery.of(context).size.width*0.1,
                 ),

                 Column(
                   children: <Widget>[
                     ElevatedButton(
                       onPressed: () {

                         setState(() {
                           EasyLocalization.of(context).locale = Locale('en','EN');
                         });

                       },child: Text("English"),),

                     ElevatedButton(
                       onPressed: () {

                         setState(() {
                           EasyLocalization.of(context).locale = Locale('bn','BN');
                         });

                       },child: Text("বাংলা"),)


                   ],
                 )

               ],
             )
             ,
           )

            ],
          ),
        )

      ),



    );
  }
}
