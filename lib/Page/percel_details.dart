import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:pickupman/Class/delivery_data.dart';
import 'package:pickupman/Color_Me/Color_me.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pickupman/Page/complete_parcel.dart';
import 'package:pickupman/widgets/btn_widget.dart';

class Percel_details extends StatefulWidget {
  @override
  _Percel_detailsState createState() => _Percel_detailsState();
}

class _Percel_detailsState extends State<Percel_details> {
  final GlobalKey<RefreshIndicatorState> refresh =
      GlobalKey<RefreshIndicatorState>();

  DeliveryData data;
  bool loading = true;

  @override
  Widget build(BuildContext context) {
    data = ModalRoute.of(context).settings.arguments;
    var deliveryStatus;
    if(data.deliveryStatus == "0"){
      deliveryStatus = 'delivery order';
    } else{
      deliveryStatus = 'delivery complete';
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color_me.white,
        title: Text(
          "Percel Details".tr().toString(),
          style: TextStyle(color: Color_me.green, fontWeight: FontWeight.bold),
        ),
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            gray_lebel("About Parcel".tr().toString()),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.01,
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Expanded(
                      flex: 1,
                      child: Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Creation Date".tr().toString(),
                          style: GoogleFonts.lato(
                              textStyle:
                                  TextStyle(fontWeight: FontWeight.bold)),
                        ),
                      )),
                  Expanded(
                      flex: 1,
                      child: Container(
                        alignment: Alignment.centerRight,
                        child: SelectableText(
                          data.date,
                          textAlign: TextAlign.end,
                          style: TextStyle(),
                        ),
                      ))
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Expanded(
                      flex: 1,
                      child: Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Shop Name".tr().toString(),
                          style: GoogleFonts.lato(
                              textStyle:
                                  TextStyle(fontWeight: FontWeight.bold)),
                        ),
                      )),
                  Expanded(
                      flex: 1,
                      child: Container(
                        alignment: Alignment.centerRight,
                        child: SelectableText(data.shopname,
                            textAlign: TextAlign.end),
                      ))
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Expanded(
                      flex: 1,
                      child: Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Status".tr().toString(),
                          style: GoogleFonts.lato(
                              textStyle:
                                  TextStyle(fontWeight: FontWeight.bold)),
                        ),
                      )),
                  Expanded(
                      flex: 1,
                      child: Container(
                        alignment: Alignment.centerRight,
                        child: SelectableText(deliveryStatus,
                            textAlign: TextAlign.end),
                      ))
                ],
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.01,
            ),
            gray_lebel("Pickup Info".tr().toString()),
            Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Expanded(
                      flex: 1,
                      child: Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Pickup Info".tr().toString(),
                          style: GoogleFonts.lato(
                              textStyle:
                                  TextStyle(fontWeight: FontWeight.bold)),
                        ),
                      )),
                  Expanded(
                      flex: 1,
                      child: Container(
                        alignment: Alignment.centerRight,
                        child: SelectableText(data.customerInfo,
                            textAlign: TextAlign.end),
                      ))
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Expanded(
                      flex: 1,
                      child: Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Tracking ID".tr().toString(),
                          style: GoogleFonts.lato(
                              textStyle:
                                  TextStyle(fontWeight: FontWeight.bold)),
                        ),
                      )),
                  Expanded(
                      flex: 1,
                      child: Container(
                        alignment: Alignment.centerRight,
                        child: SelectableText(data.trackId,
                            textAlign: TextAlign.end),
                      ))
                ],
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.01,
            ),
            gray_lebel("Payment Info".tr().toString()),
            Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Expanded(
                      flex: 1,
                      child: Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Payment Info".tr().toString(),
                          style: GoogleFonts.lato(
                              textStyle:
                                  TextStyle(fontWeight: FontWeight.bold)),
                        ),
                      )),
                  Expanded(
                      flex: 1,
                      child: Container(
                        alignment: Alignment.centerRight,
                        child: SelectableText(data.paymentinfo,
                            textAlign: TextAlign.end),
                      ))
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Expanded(
                      flex: 1,
                      child: Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Instruction".tr().toString(),
                          style: GoogleFonts.lato(
                              textStyle:
                                  TextStyle(fontWeight: FontWeight.bold)),
                        ),
                      )),
                  Expanded(
                      flex: 1,
                      child: Container(
                        alignment: Alignment.centerRight,
                        child: SelectableText(data.instruction,
                            textAlign: TextAlign.end),
                      ))
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Expanded(
                      flex: 1,
                      child: Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Payment Status".tr().toString(),
                          style: GoogleFonts.lato(
                              textStyle:
                                  TextStyle(fontWeight: FontWeight.bold)),
                        ),
                      )),
                  Expanded(
                      flex: 1,
                      child: Container(
                        alignment: Alignment.centerRight,
                        child: SelectableText(data.paymentinfo,
                            textAlign: TextAlign.end),
                      ))
                ],
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            data.deliveryStatus == '1'
                ? Container()
                : Padding(
                    padding: const EdgeInsets.all(16),
                    child: ButtonWidget(
                      btnText: 'Verify to complete order',
                      onClick: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => CompleteParcel(
                            parcelId: data.percelid,
                          ),
                        ),
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }

  Widget gray_lebel(String txt) {
    return Container(
      padding: EdgeInsets.only(left: 10),
      alignment: Alignment.centerLeft,
      color: Colors.black12,
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.04,
      child: Text(txt,
          style: GoogleFonts.lato(textStyle: TextStyle(color: Color_me.grey))),
    );
  }
}
