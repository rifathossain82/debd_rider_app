import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pickupman/Color_Me/Color_me.dart';
import 'package:pickupman/Imp/route.dart';
import 'package:easy_localization/easy_localization.dart';

class Service extends StatefulWidget {
  @override
  _ServiceState createState() => _ServiceState();
}

class _ServiceState extends State<Service> {





  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        backgroundColor: Color_me.white,

        leading: new IconButton(
            icon: new Icon(Icons.arrow_back_outlined, color: Colors.black,),
            onPressed: () =>
                Navigator.of(context)
                    .pushNamedAndRemoveUntil(
                    HOME, (Route<dynamic> route) => false)
        ),


        title: Container(
            child: Row(
              children: <Widget>[
                AutoSizeText(
                  "Service".tr().toString(),
                  style: TextStyle(
                      color: Color_me.green, fontWeight: FontWeight.w900),
                ),

              ],
            )),

      ),




      body: Center(
        child: Row(
          children: <Widget>[
            GestureDetector(
                onTap: (){

                  Navigator.of(context)
                      .pushNamedAndRemoveUntil(
                      PARCEL, (Route<dynamic> route) => false);
                },

                child: _buildItem("Delivery Info".tr().toString(),Image.asset("assets/pickup_man.png"))),
            GestureDetector(
                onTap: (){
                  Navigator.of(context)
                      .pushNamedAndRemoveUntil(
                      PICKUPMAN, (Route<dynamic> route) => false);

                },

                child: _buildItem("PickUp Info".tr().toString(),Image.asset("assets/delivery_boy.png"))),
        /*- assets/delivery_boy.png
        - assets/pickup_man.png*/
          ],
        ),


      ),
    );
  }

  Widget _buildItem(String title,Image image) {

    return FittedBox(
        child: Container(
          width: MediaQuery.of(context).size.width/2,
          height: MediaQuery.of(context).size.height/4.5,

          child: Card(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    width: 90,
                    height: 90,
                    child: image,
                  ),

                    Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          AutoSizeText(
                            title,
                            style: TextStyle(
                                fontSize: 16,
                                /*fontWeight: FontWeight.bold*/
                                color: Colors.black
                            ),
                          ),

                        ],
                      ),
                    ),

                ],
              ),
            ),
          ),

        ),
      );

  }

}
