import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:pickupman/Color_Me/Color_me.dart';
import 'package:pickupman/Design/CustomDialog.dart';
import 'package:pickupman/Design/helper.dart';
import 'package:pickupman/Imp/url.dart';
import 'package:pickupman/widgets/pin_code_textField.dart';
import 'package:http/http.dart' as http;

class CompleteParcel extends StatefulWidget {
  final String parcelId;

  CompleteParcel({Key key, this.parcelId}) : super(key: key);

  @override
  State<CompleteParcel> createState() => _CompleteParcelState();
}

class _CompleteParcelState extends State<CompleteParcel> {
  bool loading = true;
  String customerPhoneNumber = '';
  String merchantPhoneNumber = '';
  final pinController = TextEditingController();

  ///initially I set session time of otp is 180 seconds or 3 minutes
  int seconds = 180;

  void getPhoneNumbers() async {
    final response = await http
        .get(main_url + 'fetchCustomerMarchantMobileNo/' + widget.parcelId);
    var data = json.decode(response.body.toString());

    print(data);
    customerPhoneNumber = data['cus_phone'];
    merchantPhoneNumber = data['marchant_phone'];

    loading = false;

    if (mounted) {
      setState(() {});
    }
  }

  @override
  void initState() {
    getPhoneNumbers();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color_me.white,
        title: Text(
          'Verify to complete order',
          style: TextStyle(
            color: Color_me.green,
            fontWeight: FontWeight.bold,
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
      ),
      body: loading
          ? CustomDialog()
          : Padding(
              padding: const EdgeInsets.all(15),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    _buildPhoneWidget(
                        'Customer Phone No.', customerPhoneNumber, sendOTPToCustomer),
                    const SizedBox(
                      height: 25,
                    ),
                    _buildPhoneWidget(
                        'Merchant Phone No.', merchantPhoneNumber, sendOTPToMerchant),
                    const SizedBox(
                      height: 40,
                    ),
                    _buildOTPSection(size),
                    SizedBox(
                      height: size.height * 0.35,
                    ),
                    _buildDeliveryCompleteButton(size),
                  ],
                ),
              ),
            ),
    );
  }

  Widget _buildPhoneWidget(String title, String phone, Function sendOTP) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(title),
        const SizedBox(
          height: 8,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Container(
                child: Text(
                  phone,
                  style: TextStyle(color: Color_me.grey),
                ),
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 12),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: Colors.black12,
                    border: Border.all(
                      color: Color_me.borderColor,
                      width: 1,
                    )),
              ),
            ),
            const SizedBox(
              width: 16,
            ),
            GestureDetector(
              onTap: sendOTP,
              child: Container(
                child: Text(
                  'Send OTP',
                  style: TextStyle(color: Color_me.white),
                ),
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 12),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  color: Color_me.green,
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildOTPSection(Size size) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text('Enter OTP'),
        const SizedBox(
          height: 15,
        ),
        SizedBox(
          width: size.width * 0.8,
          child: PintFiledWidget(
            labelText: 'Pin',
            fieldController: pinController,
            icon: Icons.visibility,
          ),
        ),
      ],
    );
  }

  Widget _buildDeliveryCompleteButton(Size size) {
    return GestureDetector(
      onTap: verifyUser,
      child: Container(
        width: size.width,
        alignment: Alignment.center,
        child: Text(
          'Delivery Complete',
          style: TextStyle(color: Color_me.white),
        ),
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          color: Color_me.green,
        ),
      ),
    );
  }

  void sendOTPToCustomer() async {
    final response = await http
        .post(main_url + 'sendOtpToCustomer/' + widget.parcelId);
    var data = json.decode(response.body.toString());

    print(data);
    if(data.toString().contains('true')){
      showSnackBar(context, 'OTP sent to customer!', Color_me.successColor);
    }else{
      showSnackBar(context, 'OTP sent failed!', Color_me.failedColor);
    }
    if (mounted) {
      setState(() {});
    }
  }

  void sendOTPToMerchant() async {
    final response = await http
        .post(main_url + 'sendOtpToMarchant/' + widget.parcelId);
    var data = json.decode(response.body.toString());

    print(data);
    if(data.toString().contains('true')){
      showSnackBar(context, 'OTP sent to merchant!', Color_me.successColor);
    }else{
      showSnackBar(context, 'OTP sent failed!', Color_me.failedColor);
    }


    if (mounted) {
      setState(() {});
    }
  }

  void verifyUser() async {
    if(pinController.text.isEmpty){
      showSnackBar(context, 'Pin is Empty!', Color_me.failedColor);
    }else{
      var map = {};
      map['edit_id'] = widget.parcelId;
      map['otp_code'] = pinController.text;

      print('Request Data: $map');
      final response = await http
          .post(main_url + 'updateVerificationAndDelivery', body: map);
      var data = json.decode(response.body.toString());

      print(data);
      if(data['success'].toString().contains('true')){
        showSnackBar(context, data['messages'], Color_me.successColor);
        Navigator.of(context)..pop()..pop();
      }else{
        showSnackBar(context, data['messages'], Color_me.failedColor);
      }


      if (mounted) {
        setState(() {});
      }
    }
  }
}
