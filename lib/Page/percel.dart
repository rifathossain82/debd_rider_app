import 'dart:convert';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:pickupman/Class/delivery_data.dart';
import 'package:pickupman/Color_Me/Color_me.dart';
import 'package:pickupman/Design/CustomDialog.dart';
import 'package:http/http.dart' as http;

import 'package:easy_localization/easy_localization.dart';
import 'package:pickupman/Imp/route.dart';
import 'package:pickupman/Imp/url.dart';
import 'package:pickupman/main.dart';

class All_parcel extends StatefulWidget {
  @override
  _All_parcelState createState() => _All_parcelState();
}

class _All_parcelState extends State<All_parcel> {
  bool loading = true;
  List<DeliveryData> deliveryData = [];

  TextEditingController _searchQueryController = TextEditingController();
  bool _isSearching = false;
  String searchQuery = "Search query";

  final GlobalKey<RefreshIndicatorState> refresh =
      GlobalKey<RefreshIndicatorState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    get_data();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Color_me.white,
        leading: new IconButton(
            icon: new Icon(
              Icons.arrow_back_outlined,
              color: Colors.black,
            ),
            onPressed: () => Navigator.of(context).pushNamedAndRemoveUntil(
                HOME, (Route<dynamic> route) => false)),
        title: Container(
            child: Row(
          children: <Widget>[
            AutoSizeText(
              "All Parcel".tr().toString(),
              style:
                  TextStyle(color: Color_me.green, fontWeight: FontWeight.w900),
            ),
          ],
        )),
      ),
      body: RefreshIndicator(
        onRefresh: get_data,
        key: refresh,
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Container(
            padding: EdgeInsets.all(10),
            child: NotificationListener<OverscrollIndicatorNotification>(
              onNotification: (overscroll) {
                overscroll.disallowGlow();
              },
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      child: deliveryData != null
                          ? ListView.builder(
                              itemCount: deliveryData.length,
                              shrinkWrap: true,
                              //physics: NeverScrollableScrollPhysics(),
                              itemBuilder: (context, index) {
                                var deliveryStatus;
                                if(deliveryData[index].deliveryStatus == "0"){
                                  deliveryStatus = 'delivery order';
                                } else{
                                  deliveryStatus = 'delivery complete';
                                }
                                return Column(
                                  children: <Widget>[
                                    GestureDetector(
                                      onTap: () async {
                                        await Navigator.of(context).pushNamed(
                                          PERCEL_DETAILS,
                                          arguments: deliveryData[index],
                                        );
                                        var p = await Request_Notification(
                                            index);
                                        Map<String, dynamic> jsonResponse = p;

                                        if (jsonResponse
                                            .containsKey("true")) {
                                          Show_Snackbar(
                                              p['messages'], _scaffoldKey);
                                          // Navigator.of(context).pop();
                                        }

                                        get_data();
                                        setState(() {});
                                      },
                                      child: Card(
                                        child: Stack(
                                          children: <Widget>[
                                            deliveryData[index]
                                                .notification ==
                                                '0'
                                                ? Container(
                                              padding:
                                              EdgeInsets.all(10),
                                              color: Color_me.green
                                                  .withAlpha(40),
                                              width:
                                              MediaQuery
                                                  .of(context)
                                                  .size
                                                  .width,
                                              child: Column(
                                                mainAxisAlignment:
                                                MainAxisAlignment
                                                    .center,
                                                crossAxisAlignment:
                                                CrossAxisAlignment
                                                    .start,
                                                children: <Widget>[
                                                  Text(
                                                    deliveryData[index]
                                                        .shopname,
                                                    style: TextStyle(
                                                        fontSize: 18,
                                                        color: Color_me
                                                            .red),
                                                  ),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  Text(
                                                    deliveryData[index]
                                                        .date,
                                                    style: TextStyle(
                                                        fontSize: 18),
                                                  ),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  Text(
                                                    deliveryStatus,
                                                    style: TextStyle(
                                                        fontSize: 18),
                                                  )
                                                ],
                                              ),
                                            )
                                                : Container(
                                              padding:
                                              EdgeInsets.all(10),
                                              color: Colors.white,
                                              width:
                                              MediaQuery
                                                  .of(context)
                                                  .size
                                                  .width,
                                              child: Column(
                                                mainAxisAlignment:
                                                MainAxisAlignment
                                                    .center,
                                                crossAxisAlignment:
                                                CrossAxisAlignment
                                                    .start,
                                                children: <Widget>[
                                                  deliveryData[index]
                                                      .shopname !=
                                                      null
                                                      ? Text(
                                                    deliveryData[
                                                    index]
                                                        .shopname,
                                                    style: TextStyle(
                                                        fontSize:
                                                        18,
                                                        color: Color_me
                                                            .red),
                                                  )
                                                      : Container(),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  Text(
                                                    deliveryData[index]
                                                        .date,
                                                    style: TextStyle(
                                                        fontSize: 18),
                                                  ),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  Text(
                                                    deliveryStatus,
                                                    style: TextStyle(
                                                        fontSize: 18),
                                                  )
                                                ],
                                              ),
                                            ),
                                            Positioned(
                                                right: 100,
                                                top: 32,
                                                left: 300,
                                                child: Icon(
                                                  Icons.arrow_forward_ios,
                                                  color: Colors.grey,
                                                  size: 30,
                                                ))
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                );
                              }
                            )
                          : CustomDialog(),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<List> get_data() async {
    deliveryData = [];
    final response = await http.get(
        main_url + 'fetchDeliveryData/' + prefs.getString("uid1").toString());
    var responseBody = json.decode(response.body.toString());

    print(responseBody);
    for (Map<String, dynamic> data in responseBody['data']) {
      deliveryData.add(DeliveryData.fromJson(data));
    }

    loading = false;

    if (mounted) {
      setState(() {});
    }
  }

  Widget _buildSearchField() {
    return TextField(
      controller: _searchQueryController,
      autofocus: true,
      decoration: InputDecoration(
        hintText: "Search Data...",
        border: InputBorder.none,
        hintStyle: TextStyle(color: Colors.white30),
      ),
      style: TextStyle(color: Colors.white, fontSize: 16.0),
      onChanged: (query) => updateSearchQuery(query),
    );
  }

  List<Widget> _buildActions() {
    if (_isSearching) {
      return <Widget>[
        IconButton(
          icon: const Icon(Icons.clear),
          onPressed: () {
            if (_searchQueryController == null ||
                _searchQueryController.text.isEmpty) {
              Navigator.pop(context);
              return;
            }
            _clearSearchQuery();
          },
        ),
      ];
    }

    return <Widget>[
      IconButton(
        icon: const Icon(Icons.search),
        onPressed: _startSearch,
      ),
    ];
  }

  void _startSearch() {
    ModalRoute.of(context)
        .addLocalHistoryEntry(LocalHistoryEntry(onRemove: _stopSearching));

    setState(() {
      _isSearching = true;
    });
  }

  void updateSearchQuery(String newQuery) {
    setState(() {
      searchQuery = newQuery;
    });
  }

  void _stopSearching() {
    _clearSearchQuery();

    setState(() {
      _isSearching = false;
    });
  }

  void _clearSearchQuery() {
    setState(() {
      _searchQueryController.clear();
      updateSearchQuery("");
    });
  }

  void Show_Snackbar(String data, var key) {
    key.currentState.showSnackBar(SnackBar(
      content: Text(data),
      duration: Duration(seconds: 3),
    ));
  }

  Future Request_Notification(int index) async {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return CustomDialog();
      },
    );

    final response =
        await http.post(main_url + 'percelNotificationUpdate/', body: {
      'percel_id': deliveryData[index].percelid.toString(),
      'notification': "1",
    });
    Navigator.of(context).pop();
    print(response.body.toString());
    return json.decode(response.body);
  }
}
