import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:pickupman/Class/pickup_data.dart';
import 'package:pickupman/Color_Me/Color_me.dart';
import 'package:pickupman/Design/CustomDialog.dart';
import 'package:pickupman/Imp/route.dart';
import 'package:pickupman/Imp/url.dart';
import 'package:http/http.dart' as http;
import 'package:easy_localization/easy_localization.dart';

import '../main.dart';

class Pickup_man extends StatefulWidget {
  @override
  _Pickup_manState createState() => _Pickup_manState();
}

class _Pickup_manState extends State<Pickup_man> {

  List<PickUpData> pickupDataList = [];
  bool loading = true;



  @override
  void initState() {
    get_data();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        backgroundColor: Color_me.white,

        leading: new IconButton(
            icon: new Icon(Icons.arrow_back_outlined, color: Colors.black,),
            onPressed: () =>
                Navigator.of(context)
                    .pushNamedAndRemoveUntil(
                    HOME, (Route<dynamic> route) => false)
        ),


        title: Container(
            child: Row(
              children: <Widget>[
                AutoSizeText(
                  "All Parcel".tr().toString(),
                  style: TextStyle(
                      color: Color_me.green, fontWeight: FontWeight.w900),
                ),

              ],
            )),

      ),


      body: RefreshIndicator(
        onRefresh: get_data,

        child: Container(
          width: MediaQuery
              .of(context)
              .size
              .width,
          height: MediaQuery
              .of(context)
              .size
              .height,

          child: Container(
            padding: EdgeInsets.all(10),
            child: NotificationListener<OverscrollIndicatorNotification>(
              onNotification: (overscroll) {
                overscroll.disallowGlow();
              },
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[

                    Container(
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      height: MediaQuery
                          .of(context)
                          .size
                          .height,
                      child: pickupDataList != null
                          ? ListView.builder(
                        itemCount: pickupDataList.length,
                        shrinkWrap: true,
                        //physics: NeverScrollableScrollPhysics(),
                        itemBuilder: (context, index) {
                          if(pickupDataList[index].pickupStatus == '0'){
                            return Column(
                              children: <Widget>[
                                GestureDetector(
                                    onTap: () async{
                                      await Navigator.of(context).pushNamed(PICKUPMAN_DETAILS,
                                          arguments: pickupDataList[index]);

                                      var p = await Request_Notification(index);
                                      Map<String, dynamic> jsonResponse = p;

                                      if (jsonResponse.containsKey("true")) {
                                        // Navigator.of(context).pop();
                                      }

                                      get_data();
                                      setState(() {});

                                    },

                                    child:
                                    Card(

                                      child: Stack(
                                        children: <Widget>[
                                          pickupDataList[index].notification == '0'
                                              ? Container(

                                            padding: EdgeInsets.all(10),
                                            color: Color_me.green.withAlpha(40),
                                            width: MediaQuery
                                                .of(context)
                                                .size
                                                .width,
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              crossAxisAlignment: CrossAxisAlignment
                                                  .start,
                                              children: <Widget>[
                                                Text(pickupDataList[index].shopname,
                                                  style: TextStyle(
                                                      fontSize: 18,
                                                      color: Color_me.red
                                                  ),),
                                                SizedBox(height: 5,),
                                                Text(pickupDataList[index].date,
                                                  style: TextStyle(
                                                      fontSize: 18
                                                  ),),

                                                SizedBox(height: 5,),
                                                Text(pickupDataList[index].status,
                                                  style: TextStyle(
                                                      fontSize: 18
                                                  ),)

                                              ],
                                            ),
                                          )
                                              : Container(

                                            padding: EdgeInsets.all(10),
                                            color: Colors.white,
                                            width: MediaQuery
                                                .of(context)
                                                .size
                                                .width,
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              crossAxisAlignment: CrossAxisAlignment
                                                  .start,
                                              children: <Widget>[
                                                pickupDataList[index].shopname != null
                                                    ?Text(pickupDataList[index].shopname,
                                                  style: TextStyle(
                                                      fontSize: 18,
                                                      color: Color_me.red
                                                  ),):Container(),
                                                SizedBox(height: 5,),
                                                Text(pickupDataList[index].date,
                                                  style: TextStyle(
                                                      fontSize: 18
                                                  ),),

                                                SizedBox(height: 5,),
                                                Text(pickupDataList[index].status,
                                                  style: TextStyle(
                                                      fontSize: 18
                                                  ),)

                                              ],
                                            ),
                                          ),
                                          Positioned(

                                              right: 100,
                                              top: 32,
                                              left: 300,


                                              child: Icon(Icons.arrow_forward_ios,
                                                color: Colors.grey,size: 30,))


                                        ],
                                      ),

                                    )

                                )
                              ],
                            );
                          }
                          else{
                            return Container();
                          }
                        },
                      ):CustomDialog(),
                    ),

                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<List> get_data() async {
    pickupDataList = [];

    final response = await http.get(main_url+'fetchPercelDataforpickup/'+ prefs.getString("uid1").toString());
    print(response.statusCode);
    print(response.body);
    var responseBody = json.decode(response.body.toString());

    for(Map<String, dynamic> data in responseBody['data']){
      pickupDataList.add(PickUpData.fromJson(data));
    }

    loading = false;

    if (mounted) {
      setState(() {});
    }
  }


  Future Request_Notification(int index)async {

    showDialog(context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return CustomDialog();
        },
    );


    final response =
    await http.post(main_url+'percelNotificationUpdate/', body: {
      'percel_id':pickupDataList[index].id.toString(),
      'notification': "1",
    });
    Navigator.of(context).pop();
    print(response.body.toString());
    return json.decode(response.body);



  }



}
