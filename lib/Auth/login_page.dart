import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pickupman/Color_Me/Color_me.dart';
import 'package:pickupman/Design/CustomDialog.dart';
import 'package:pickupman/Imp/route.dart';
import 'package:pickupman/Imp/url.dart';
import 'package:pickupman/widgets/btn_widget.dart';
import 'package:pickupman/widgets/herder_container.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:http/http.dart' as http;

import '../main.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  bool button_state = false;

  TextEditingController full_name = new TextEditingController();
  TextEditingController user_email = new TextEditingController();
  TextEditingController phone_number = new TextEditingController();
  TextEditingController user_address = new TextEditingController();
  TextEditingController user_name = new TextEditingController();
  TextEditingController user_password = new TextEditingController();

  String f_name, u_email, p_number, u_address, u_name, u_password;

  bool forget_pass_state = false;
  double button_width = 170;

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  String token1;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    firebaseCloudMessaging_Listeners();
/*    _firebaseMessaging.getToken().then((token){

      print("token is"+ token);
      token1 = token;
      setState(() {

      });
    });*/
  }

  void firebaseCloudMessaging_Listeners() {
    _firebaseMessaging.getToken().then((token){

      print("token is "+ token);
      token1 = token;
      print("2nd "+token1);
      setState(() {

      });
    }


    );
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,

        child: Form(
          key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                HeaderContainer("Login"),
                Expanded(
                  flex: 1,
                  child: Container(
                    margin: EdgeInsets.only(left: 20, right: 20, top: 30),
                      child: ListView(
                        shrinkWrap: true,
                        children: <Widget>[
                          _textInput(TextInputType.text,user_name,"User Name",u_name,Icon(Icons.person)),
                          _textPassword(TextInputType.visiblePassword,user_password,"Password",u_password,Icon(Icons.vpn_key)),
                           SizedBox(
                             height: MediaQuery.of(context).size.height*0.1,
                           ),
                             Center(
                               child: ButtonWidget(
                                 onClick: () {
                                   if (_formKey.currentState.validate()) {
                                     showDialog(
                                       context: context,
                                       builder: (BuildContext context) {
                                         return CustomDialog();
                                       },
                                     );
                                     GO();
                                   }
                                 },
                                 btnText: "LOGIN",
                               ),
                            ),



                           ],
                      ),
                  ),
                ),


              ],
            ),
        ),
      ),
    );
  }





  Widget _textInput(var key_type, TextEditingController Controller, String hint,
      String controll, var icon) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(20)),
        color: Colors.white,
      ),
      padding: EdgeInsets.only(left: 10),
      child: TextFormField(
        keyboardType:key_type,
        controller: Controller,
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: hint,
          prefixIcon: icon,
        ),
        validator: (String value) {
          if (value.isEmpty) {
            return 'Please Enter Your '+hint;
          } else {
            return null;
          }
        },
        onSaved: (String value) {
          controll = value;
        },


      ),
    );
  }

  Widget _textPassword(var key_type, TextEditingController Controller, String hint,
      String controll, var icon) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(20)),
        color: Colors.white,
      ),
      padding: EdgeInsets.only(left: 10),
      child: TextFormField(
        obscureText: true,
        keyboardType:key_type,
        controller: Controller,
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: hint,
          prefixIcon: icon,
        ),
        validator: (String value) {
          if (value.isEmpty) {
            return 'Please Enter Your '+hint;
          } else {
            return null;
          }
        },
        onSaved: (String value) {
          controll = value;
        },


      ),
    );
  }


  void Show_Snackbar(String data) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(data),
      duration: Duration(seconds: 3),
    ));
  }



  void GO() async {
    var p = await request_Login();

    button_state = false;
    button_width = 170;

    setState(() {});

    Map<String, dynamic> js = p;
    if (js.containsKey('errors')) {
      Show_Snackbar(p['errors']);
    } else {
      Store(p, context);
      Navigator.of(context)
          .pushNamedAndRemoveUntil(HOME, (Route<dynamic> route) => false);
    }
  }


  Future request_Login() async {
    final response = await http.post(
        main_url+'PickupUserlogin/',
        body: {
          'username': user_name.text,
          'password': user_password.text,
          'key': token1.toString(),
        });
    Navigator.of(context).pop();
    print(token1.toString());
    return json.decode(response.body);
  }


  Store(var mat, BuildContext context) async {
    prefs = await SharedPreferences.getInstance();
    prefs.setString('uid1', mat['info']['uid1'].toString());


    button_state = false;
    button_width = 170;
    setState(() {});
    Navigator.of(context)
        .pushNamedAndRemoveUntil(HOME, (Route<dynamic> route) => false);
  }




}
