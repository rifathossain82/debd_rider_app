import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pickupman/Color_Me/Color_me.dart';
import 'package:pickupman/Imp/route.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';

class AnimatedSplashScreen extends StatefulWidget {
  @override
  _AnimatedSplashScreenState createState() => _AnimatedSplashScreenState();
}

class _AnimatedSplashScreenState extends State<AnimatedSplashScreen> {
  @override
  void initState() {
    super.initState();

    SharedPreferences.getInstance().then((pr) {
      prefs=pr;
    });
/*    animationController = new AnimationController(
        vsync: this, duration: new Duration(milliseconds: 1500));
    animation =
    new CurvedAnimation(parent: animationController, curve: Curves.easeOut);

    animation.addListener(() => this.setState(() {}));
    animationController.forward();*/

    setState(() {
      _visible = !_visible;
    });
    startTime();
  }



  var _visible = true;

  /* AnimationController animationController;
  Animation<double> animation;*/

  startTime() async {
    var _duration = new Duration(milliseconds: 3500);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() async{
    if(prefs.containsKey('uid1'))
    {
      Navigator.of(context).pushReplacementNamed(HOME);
    }
    else
    {
      Navigator.of(context).pushReplacementNamed(LOG_IN);
    }
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Color_me.white,
      body: Container(
        height: size.height,
         width: size.width,
         alignment: Alignment.center,
          child: Stack(
            children: <Widget>[
              Container(
                height: size.height,
                padding: EdgeInsets.all(15),
                  alignment: Alignment.center,
                  child:Column(
                    children: <Widget>[
                          SizedBox(
                            height: 200,
                          ),
                          Image.asset('assets/slash.png'),
                          Image.asset(
                            'assets/logo.png',
                            width: size.width * 0.7,
                          )
                    ],
                  )

              ),
            ],
          )
      ),
    );
  }
}
