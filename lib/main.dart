import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:pickupman/Auth/login_page.dart';
import 'package:pickupman/Color_Me/Color_me.dart';
import 'package:pickupman/Design/CustomDialog.dart';
import 'package:http/http.dart' as http;
import 'package:pickupman/Imp/route.dart';
import 'package:pickupman/Imp/url.dart';
import 'package:pickupman/Page/data.dart';
import 'package:pickupman/Page/percel_details.dart';
import 'package:pickupman/Page/pickup_details.dart';
import 'package:pickupman/Page/pickup_man.dart';
import 'package:pickupman/Page/settings_page.dart';
import 'package:pickupman/Slash_screen/slash_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Page/percel.dart';

class MyHttpOverrides extends HttpOverrides{
  @override
  HttpClient createHttpClient(SecurityContext context){
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port)=> true;
  }
}

SharedPreferences prefs;

Future<void> main() async{
  WidgetsFlutterBinding.ensureInitialized();
  // await EasyLocalization.ensureInitialized();
  await Firebase.initializeApp();
  HttpOverrides.global = new MyHttpOverrides();
  runApp(EasyLocalization(
    child: MyApp(),
    path: 'Language',
    saveLocale: true,
    supportedLocales: [
      Locale('en','EN'),
      Locale('bn','BN'),
    ],
  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'DEBD Rider',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),

      // localizationsDelegates: context.localizationDelegates,
      // // supportedLocales: context.supportedLocales,
      // locale: context.locale,

      home: MyHomePage(title: 'DEBD Rider'),

      routes: <String, WidgetBuilder>{
        HOME: (BuildContext context) => MyHomePage(),
        PARCEL: (BuildContext context) => All_parcel(),
        PERCEL_DETAILS: (BuildContext context) => Percel_details(),
        LOG_IN: (BuildContext context) => LoginPage(),
        SLASH_SCREEN: (BuildContext context) => AnimatedSplashScreen(),
        SERVICE: (BuildContext context) => Service(),
        PICKUPMAN: (BuildContext context) => Pickup_man(),
        PICKUPMAN_DETAILS: (BuildContext context) => Pickup_details(),
        SETTINGS: (BuildContext context) => Settings(),



      },
      initialRoute:SLASH_SCREEN,



    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);


  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var data;
  bool loading = true;
  Timer timer;


  @override
  void initState() {
    timer = Timer.periodic(Duration(seconds: 5), (Timer t) => addValue());

    get_posts();
    // TODO: implement initState
    super.initState();
  }

  void addValue() {
    setState(() {
      get_posts();
    });
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color_me.white,
        shadowColor: Color_me.green,


        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),

        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Container(
          alignment: Alignment.centerLeft,
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 7,
                  child: Container(
                    alignment: Alignment.centerLeft,
                      width: MediaQuery.of(context).size.width*0.1,
                      height: MediaQuery.of(context).size.height*0.03,
                      child: Image.asset('assets/logo.png')
                  ),
                ),
                Expanded(
                    flex: 1,
                    child: GestureDetector(
                      onTap: ()
                      {
                        Navigator.pop(context);
                        Navigator.of(context).pushNamed(SERVICE);

                      },
                      child: Container(
                        width: 20,
                  alignment: Alignment.centerRight,
                  child: Row(
                      children: <Widget>[
                        Stack(
                          clipBehavior: Clip.none,
                          children: [
                            Icon(Icons.notifications,color: Color_me.green,size: 30,),
                            Positioned(
                              bottom: 16,
                                left: 17,
                                child: Container(
                                  alignment: Alignment.center,
                                    height: 15,
                                    width: 15,
                                    decoration: BoxDecoration(
                                      color: Colors.red,
                                      shape: BoxShape.circle,
                                    ),
                                    child:
                                    data!=null
                                    ?Text(jsonEncode(data['total_notification']),style: TextStyle(color: Colors.white,fontSize: 8),)
                                    :Container()
                                ))
                          ],
                        )
                      ],
                  )
                ),
                    ))
              ],
            )
        ),


        /*Image.asset(
          "assets/worldex.png",
          width: 150,

        ),*/
      ),
      body: loading
      ?CustomDialog()
      :Container(

            child: SingleChildScrollView(
              child: Column(

                children: <Widget>[
                   Row(
                     children: <Widget>[
                       _buildItem("Pickup".tr().toString(),jsonEncode(data['pickup']), Color_me.mainColor,  Image.asset(
                         "assets/pickup_truck.png",
                       )
                       ),
                       _buildItem("picked".tr().toString(),jsonEncode(data['picked']), Color_me.mainColor,  Image.asset(
                         "assets/pickup_truck.png",
                       )
                       ),
                     ],
                   ),
                  Row(
                    children: <Widget>[
                      _buildItem("Delivery".tr().toString(),jsonEncode(data['delivery']), Color_me.mainColor,  Image.asset(
                        "assets/proccessing.png",
                      ),
                      ),
                      _buildItem("Completed".tr().toString(),jsonEncode(data['completed']), Color_me.mainColor,  Image.asset(
                        "assets/checked.png",
                      ),
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      _buildItem("Returned".tr().toString(),jsonEncode(data['returned']), Color_me.mainColor,  Image.asset(
                        "assets/returning.png",
                      ),
                      ),
                      _buildItem("Paid".tr().toString(),jsonEncode(data['paid']), Color_me.mainColor,   Image.asset(
                        "assets/taka.png",
                      ),
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      _buildItem("Pay".tr().toString(),jsonEncode(data['pay']), Color_me.mainColor,  Image.asset(

                        "assets/returning.png",

                      ),

                      ),
                      _buildItem("Due".tr().toString(),jsonEncode(data['due']), Color_me.mainColor,  Image.asset(

                        "assets/taka.png",

                      ),
                      ),
                    ],
                  ),

                ],

              ),
            )


      ),
      endDrawer: Container(
        width: 250,
        child: Drawer(
          // Add a ListView to the drawer. This ensures the user can scroll
          // through the options in the drawer if there isn't enough vertical
          // space to fit everything.
          child: ListView(
            // Important: Remove any padding from the ListView.
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(

                child:Container(

                  child: Image.asset(
                    "assets/logo.png",
                    fit: BoxFit.fitWidth,
                  ),
                ),

                decoration: BoxDecoration(
                  color: Color_me.green,
                ),
              ),


              ListTile(
                title: Row(
                  children: [
                    Icon(Icons.add_business_outlined),
                    SizedBox(width: 5,),
                    AutoSizeText('Percel'.tr().toString()),
                  ],
                ),
                onTap: () {

                  Navigator.pop(context);
                  Navigator.of(context).pushNamed(SERVICE);
                  // Update the state of the app.
                  // ...
                },
              ),



              /*  Divider(
               height: 1,
               color: Colors.black,
             ),*/

              ListTile(
                title: Row(
                  children: [
                    Icon(Icons.settings),
                    SizedBox(width: 5,),
                    AutoSizeText('Settings'.tr().toString()),
                  ],
                ),

                onTap: () {

                  Navigator.pop(context);
                  Navigator.of(context).pushNamed(SETTINGS);
                  // Update the state of the app.
                  // ...
                },

              ),

              /*  Divider(
               height: 1,
               color: Colors.black,
             ),
*/

              ListTile(

                title: Row(
                  children: [
                    Icon(Icons.logout),
                    SizedBox(width: 5,),
                    AutoSizeText('Logout'.tr().toString()),
                  ],
                ),

                onTap: () {

                  prefs.clear();
                  Navigator.of(context).pushNamed(LOG_IN);

                },
              ),


            ],
          ),
        ),
      ),
// This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Widget _buildItem(String title, String subtitle, Color chartColor,Image image) {

    return Flexible(
      flex: 5,
      child: FittedBox(
        child: Container(
          width: MediaQuery.of(context).size.width/2,
          height: MediaQuery.of(context).size.height/4.5,

          child: Card(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    width: 30,
                    height: 30,
                    child: image,
                  ),

                  SizedBox(width: 10,),
                  Expanded(
                    child: Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          AutoSizeText(
                            title,
                            style: TextStyle(
                                fontSize: 16,
                                /*fontWeight: FontWeight.bold*/
                                color: Colors.black
                            ),
                          ),
                          AutoSizeText(
                            subtitle,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 14,
                              color: Colors.black,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),

        ),
      ),
    );
  }


  Future<List> get_posts() async {

    final response =
    await http.get(main_url+"fetchPercelCountDataByIdforStaff/"+prefs.getString("uid1").toString());
    data = json.decode(response.body);
    /*var nameJson = data['inhouse'];
    nameString = jsonEncode(nameJson);*/ // jsonEncode != .toString()

    print(data);

    loading = false;

    if(mounted){
      setState(() {});
    }


    /*   Link = data['next_page_url'];
    _refreshController.refreshCompleted();*/

  }




}
