import 'package:flutter/material.dart';

class Color_me{
  static Color lemon = Color(0xFFCEDA0D);
  static Color grey = Color(0xFF474747);
  static Color white = Color(0xFFFFFFFF);
  static Color of_white = Color(0xFFf8efdb);
  static Color back = Color(0xFF93de69);
  static Color red = Color(0xFFC40000);
  static Color blue = Color(0xFF3835C9);
  static Color p_red = Color(0xFFF15F5F);
  static Color p_green = Color(0xFF3BEA7B);
  static Color green = Color(0xFF05854C);
  static Color dark_blue = Color(0xFF0B394D);
  static Color wood = Color(0xFF964600);
  static Color purple = Color(0xFF3B0942);
  static Color borderColor = Colors.grey.shade400;
  static Color filledColor = Colors.grey;

  static Color successColor = Colors.green;
  static Color failedColor = Colors.red;
  static Color warningColor = Colors.orange;


  static Color mainColor = Color(0xFFda291c);
  //static Color lemon=Color(int.parse('#CEDA0D'.substring(1, 7), radix: 16) + 0xFFCEDA0D);


}